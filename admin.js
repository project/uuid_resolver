/**
 * @file
 * Admin interface javascript plugin.
 */

/**
 * Update HTTP code radios selection based on textfield input.
 */
var httpCodeUpdateSelection = function () {
  var code = $('.uuid-resolver-http-code-custom').val();
  var found = false;
  $('input.uuid-resolver-http-code-default').each(function (index) {
    if (!found) {
      if ($(this).val() == code) {
        $(this).attr('checked', 'checked');
        found = true;
      }
    }
  });
  if (!found) {
    $('input.uuid-resolver-http-code-default[value=-1]').attr('checked', 'checked');
  }
}

/**
 * Show notification for cleaning user-entered path.
 */
var hideCleanNotify = function (event) {
  var cleanNotify = $('#path-clean-notify');
  if (event.which != 13 && cleanNotify.is(':visible')) {
    cleanNotify.fadeOut(200);
  }
  return true;
}

$(document).ready(function () {
  var resolverOverviewEnabled = $('#uuid-resolver-overview-table input:checkbox.checkbox-enabled');
  if (resolverOverviewEnabled.size() > 0) {
    // Hide submit button and trigger form submit on "Enabled" checkbox change.
    var resolverForm = $('#uuid-resolver-overview-form');
    resolverOverviewEnabled.change(function () {
      resolverForm.submit();
    });
    $('#uuid-resolver-actions').hide();
  }

  // Register custom code textfield handler
  var httpCodeCustom = $('.uuid-resolver-http-code-custom');
  if (httpCodeCustom.size() > 0) {
    httpCodeCustom.focus(httpCodeUpdateSelection);
    httpCodeCustom.keyup(httpCodeUpdateSelection);
    httpCodeCustom.change(httpCodeUpdateSelection);
  }

  // Register path cleaner callback
  var resolverSettingsForm = $('.uuid-resolver-resolver-settings-form');
  if (resolverSettingsForm.size() > 0) {
    // Insert notification message container
    var cleanNotify = $('<span id="path-clean-notify">(Path cleaned. Submit again to save.)</span>')
      .css('color', '#888').css('fontSize', '87%')
      .css('marginLeft', '20px').css('display', 'inline').hide();
    cleanNotify.insertAfter($('.uuid-resolver-resolver-settings-form #edit-base-path-wrapper span.field-suffix'));

    // Register message hiding handler
    var editBasePath = $('#edit-base-path');
    editBasePath.keydown(hideCleanNotify);
    editBasePath.focus(hideCleanNotify);

    // Clean path on submit and show message
    resolverSettingsForm.submit(function () {
      var origPath = editBasePath.val();
      if (origPath != '') {
        var newPath = origPath.replace(/\s+/g, '').replace(/^\/+|\/+$/g, '').replace(/\/{2,}/g, '/');
        if (newPath != origPath) {
          editBasePath.val(newPath);
          if (!cleanNotify.is(':visible')) {
            cleanNotify.fadeIn('800');
          }
          return false;
        }
      }
      return true;
    });
  }
});